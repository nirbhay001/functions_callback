const fs = require('fs');
const path=require('path');


const readFile=((callbackFunction)=>{
    let pathFile=path.join(__dirname, 'lipsum.txt');
    fs.readFile(pathFile, 'utf8', (err, data) => {
    if (err) {
    throw new Error(err)
    }
    else{
        callbackFunction(data);
    }
});
});

const convertContentUpperCase=(data,callbackFunction)=>{
    let dataUpper = data.toUpperCase();
    let upperPathFile=path.join(__dirname, 'upperLipsum.txt');
    let filesNamePath=path.join(__dirname, 'filesNames.txt');
    fs.writeFile(upperPathFile, dataUpper,(error)=>{
        if(error){
            throw new Error(error); 
        }
        else{
            fs.writeFile(filesNamePath, upperPathFile,(error)=>{
                if(error){
                    throw new error(error);
                }
                else{
                    callbackFunction(data);
                }
            })
        }
    })
};

const convertContentLowerCase=(callbackFunction)=>{
    let filePath=path.join(__dirname,'upperLipsum.txt');
    let lowerCaseFile=path.join(__dirname, 'lowerLipsum.txt');
    fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) {
        throw new Error(err)
        }
        else{
            let lowerCaseData=data.toLowerCase();
            let dataInSentences = lowerCaseData.split('.').join('\n');
            fs.writeFile(lowerCaseFile,dataInSentences,(error)=>{
                if(error){
                    throw new Error(error)
                }
                else{
                    let lowercaseFilePath=path.join(__dirname,'lowerLipsum.txt');
                    let filesNamesPath=path.join(__dirname , 'filesNames.txt');
                    fs.writeFile(filesNamesPath,'\n'+lowercaseFilePath,{flag:'a'},(error)=>{
                        if(error){
                            throw new Error(error)
                        }
                        else{
                            callbackFunction();
                        }
                    })
                }
            })

        }
    
    });

}

const sortContentOfNewFiles=(callbackFunction)=>{
    const upperCaseDataPath=path.join(__dirname,'upperLipsum.txt');
    const lowerCaseDataPath=path.join(__dirname,'lowerLipsum.txt');
    fs.readFile(upperCaseDataPath,'utf-8', (err,data)=>{
        if(err){
            throw new Error("Error happened");
        }
        else{
            let upperCaseData=data;
            fs.readFile(lowerCaseDataPath,'utf-8',(err,data)=>{
                if(err){
                    throw new Error(err);
                }
                else{
                    let lowerCaseData=data;
                    let newData=upperCaseData + lowerCaseData;
                    let sortedData=newData.split(' ').sort().join('\n');
                    let sortedDataPathFile=path.join(__dirname , 'sortedDataFile.txt');
                    fs.writeFile(sortedDataPathFile,sortedData,(error)=>{
                        if(error){
                            throw new Error(error);
                        }
                        else{
                            let filesNamePath=path.join(__dirname,'filesNames.txt')
                            fs.writeFile(filesNamePath,'\n'+sortedDataPathFile,{flag:'a'},(error)=>{
                                if(error){
                                    throw new Error(error)
                                }
                                else{
                                    callbackFunction();
                                }
                            })
                        }
                    })
                
                }

            })
        }
    })
}

const readDeleteFiles=(callbackFunction)=>{
let filesNamePath=path.join(__dirname,'filesNames.txt');
fs.readFile(filesNamePath,'utf-8',(err,data)=>{
    if(err){
        throw new Error(err);
    }
    else{
        let dataList=data.split('\n');
        dataList.map((item)=>{
            fs.unlink(item,(error)=>{
                if(error){
                    throw Error(error);
                }
            })
        })
        callbackFunction("All file deleted");
        
    }
})


}


module.exports={readFile,convertContentUpperCase,convertContentLowerCase, sortContentOfNewFiles,readDeleteFiles};

















