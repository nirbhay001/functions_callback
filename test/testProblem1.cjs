const {directoryCreate,createRandomJsonFile,deleteFileSimultaneously} = require("../problem1.cjs");

directoryCreate('JsonDirectory',(directory)=>{
    createRandomJsonFile(directory,5,(directory)=>{
        deleteFileSimultaneously(directory,(message)=>{
            console.log(message);
        });
    });
});

