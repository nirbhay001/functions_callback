const fs = require('fs');
const path = require('path');


const directoryCreate = (nameDirectory, callbackFunction) => {
    const directoryPath = path.join(__dirname, nameDirectory);
    fs.mkdir(directoryPath, { recursive: true }, (error) => {
        if (error) {
            throw new Error(error);
        } else {
            if (typeof callbackFunction === 'function') {
                callbackFunction(directoryPath);
            }
        }
    });
    return directoryPath;
};

const createRandomJsonFile = (directoryPath, num, callbackFunction) => {

    for (let file = 1; file <= num; file++) {
        let fileName = Math.random().toString().slice(2) + '.json';
        let pathFile = path.join(directoryPath, fileName);
        fs.open(pathFile, 'w', (error) => {
            if (error) {
                throw new Error(error);
            }
        })
    }
    callbackFunction(directoryPath);

    return directoryPath;
};

const deleteFileSimultaneously = (directoryPath, callbackFunction) => {
    fs.readdir(directoryPath, (error, data) => {
        if (error) {
            throw new Error(error);
        }
        else {
            data.map((fileName) => {
                const filePath = path.join(directoryPath, fileName);
                fs.unlink(filePath, (error) => {
                    if (error) {
                        throw new Error(error);
                    }
                })
            });
            callbackFunction("All files deleted");
        }
    })
};



module.exports.directoryCreate = directoryCreate;
module.exports.createRandomJsonFile = createRandomJsonFile;
module.exports.deleteFileSimultaneously = deleteFileSimultaneously;
